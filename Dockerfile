
FROM docker-registry.default.svc:5000/doug/duke-python-openshift

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Office of Information Technology' \
      architecture='x86_64' \
      summary='lad-mirror-status reports on cronjobs in lad-mirror' \
      description='Creates a report on current CronJob status in lad-mirror' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/linux-at-duke/lad-mirror-status.git'

LABEL version='1.0' \
      release='1'

ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONUNBUFFERED=0

COPY app /app
WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 5000

CMD ["python3", "app.py"]