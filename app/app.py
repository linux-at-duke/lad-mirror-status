#!/usr/bin/env python3

'''
Reads the status of lad-mirror cronjobs and spits them onto a bad webpage
'''

from kubernetes import client, config
from openshift.dynamic import DynamicClient
import logging

from flask import Flask, jsonify

app = Flask(__name__)
logging.getLogger(__name__)
config.load_incluster_config()
oc_client = DynamicClient(client.ApiClient())


@app.route('/healthz')
def healthz():
    return jsonify({'status': 'Running'})


@app.route('/')
def show_statuses():
    cronjob_status_report = []
    v1_cronjobs = oc_client.resources.get(
        api_version='batch/v1beta1',
        kind='CronJob'
    )
    cronjob_list = v1_cronjobs.get(namespace='lad-mirror')
    for cronjob in cronjob_list.items:
        print({'item': cronjob.metadata.name})
        cronjob_status = {
            'name': cronjob.metadata.name,
            'project': cronjob.metadata.labels['lad-mirror-project'],
            'status': cronjob.status
        }
        cronjob_status_report.append(cronjob_status)
    print(cronjob_status_report)
    while cronjob_status_report is []:
        pass
    return jsonify(cronjob_status_report)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
